add_subdirectory(schemas)

# lomiri-schemas.pc

configure_file("${CMAKE_CURRENT_SOURCE_DIR}/lomiri-schemas.pc.in" "${CMAKE_CURRENT_BINARY_DIR}/lomiri-schemas.pc" @ONLY)
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/lomiri-schemas.pc" DESTINATION "${CMAKE_INSTALL_FULL_DATADIR}/pkgconfig/")

# accountsservice/*.xml

file(GLOB_RECURSE XML_FILES "${CMAKE_CURRENT_SOURCE_DIR}/accountsservice/*.xml")
install(DIRECTORY DESTINATION "${CMAKE_INSTALL_FULL_DATADIR}/accountsservice/interfaces")

foreach(XML_FILE ${XML_FILES})
    get_filename_component(XML_FILE ${XML_FILE} NAME)
    install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/accountsservice/${XML_FILE}" DESTINATION "${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/interfaces")
    install(CODE "message(STATUS \"Symlinking: \$ENV{DESTDIR}${CMAKE_INSTALL_FULL_DATADIR}/accountsservice/interfaces/${XML_FILE}\")")
    install(CODE "execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink ../../dbus-1/interfaces/${XML_FILE} \$ENV{DESTDIR}${CMAKE_INSTALL_FULL_DATADIR}/accountsservice/interfaces/${XML_FILE})")
endforeach()

# accountsservice/com.lomiri.AccountsService.policy

install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/accountsservice/com.lomiri.AccountsService.policy" DESTINATION "${CMAKE_INSTALL_FULL_DATADIR}/polkit-1/actions")

# accountsservice/50-com.lomiri.AccountsService.rules

install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/accountsservice/50-com.lomiri.AccountsService.rules" DESTINATION "${CMAKE_INSTALL_FULL_DATADIR}/polkit-1/rules.d")
