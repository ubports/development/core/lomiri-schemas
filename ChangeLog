2025-01-20 Mike Gabriel

        * Release 0.1.7 (HEAD -> main, tag: 0.1.7)
        * Merge branch 'pr/drop-pkla' into 'main' (6bcde68)

2025-01-15 Robert Tari

        * Drop the deprecated PolicyKit *.pkla file (8cee2bc)fixes:
          https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1093070

2025-01-10 Mike Gabriel

        * Merge branch 'fix/projectLanguagesDeclaration' into 'main'
          (5e6dfcc)

2025-01-04 OPNA2608

        * CMakeLists.txt: Declare that this project needs no compilers to
          build (6eec051)

2025-01-01 Mike Gabriel

        * Release 0.1.6 (0f3990a) (tag: 0.1.6)
        * Merge branch 'mariogrip-main-patch-53557' into 'main' (977f99a)

2024-12-31 Marius Gripsgard

        * Update default pinned apps (has been renamed to lomiri-*) (a1ebd43)

2024-07-12 Mike Gabriel

        * Release 0.1.5 (814c0b1) (tag: 0.1.5)

2024-05-16 Mike Gabriel

        * Merge branch 'personal/sunweaver/gsettings-background-image' into
          'main' (6ba5755)

2024-03-09 Mike Gabriel

        * data/schemas/com.lomiri.Shell.gschema.xml.in: Add
          background-picture-uri setting for user session and
          greeter. (d52a08d)

2024-05-01 Mike Gabriel

        * Merge branch 'personal/fredldotme/lightmode' into 'main' (fc794f3)

2024-03-05 Alfred Neumayer

        * data: Add setting for shell-wide light mode (11613fe)

2024-04-25 Mike Gabriel

        * Merge branch 'personal/peat-psuwit/build-against-debian' into
          'main' (e2b350f)

2024-04-24 Ratchanan Srirattanamet

        * debian/{control, rules}: adapt packaging to also build on Debian
          (68c3ff5)

2024-04-24 Mike Gabriel

        * Merge branch
          'personal/peat-psuwit/migration-dont-overwrite-settings'
          into 'main' (e4368fc)

2024-01-25 Ratchanan Srirattanamet

        * debian/session-migration: try not to overwrite settings (54745a2)

2024-04-18 Ratchanan Srirattanamet

        * Merge branch 'logo-resolver' into 'main' (775141f)

2024-03-22 Brandon Boese

        * data/schemas/com.lomiri.Shell.gschema.xml.in: Add logo-picture-uri
          (2d6accd)

2024-01-27 Mike Gabriel

        * Release 0.1.4 (6acdde9) (tag: 0.1.4)

2023-02-03 Mike Gabriel

        * data/schemas/com.lomiri.touch.system.gschema.xml.in: Support
          enabling/disabling the flashlight-as-a-torch via
          gsettings. (1b2fde9)

2024-01-05 Ratchanan Srirattanamet

        * Merge branch 'main' into 'main' (b94bbb5)

2024-01-05 Muhammad

        * schemas: rename charging information gsettin.
          show-charging-information ->
          show-charging-information-while-locked (b88d951)

2023-12-31 Muhammad

        * schemas: introduce new schema for configuring charging information
          (0ca6b0e)

2023-12-14 Ratchanan Srirattanamet

        * Merge branch 'ubports/focal_-_notif_privacymode' into 'main'
          (92055eb)

2023-08-09 Lionel Duboeuf

        * Add new property to disable content notification while locked
          (0041bc5)

2023-12-11 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/dt2w-bumpversion' into 'main'
          (7571e01)

2023-12-05 Ratchanan Srirattanamet

        * debian/changelog: bump version to allow versioned depends (6f998f8)
        * Merge branch 'ubports/focal_-_dt2w_persist' into 'main' (a75a4dc)

2023-12-01 Lionel Duboeuf

        * add gsetting for double tap to wake (586c756)

2023-02-15 Mike Gabriel

        * Release 0.1.3 (fed68d6) (tag: 0.1.3)
        * Merge branch 'personal/lduboeuf/focal_-_pincode_increase_length'
          into 'main' (8f42732)

2023-02-14 Lionel Duboeuf

        * store pincode length For UI/UX backward compatibility, we need to
          store the length of the pinCode so that we can have a
          placeholder and auto-login feature possible (adf5dc7)

2023-02-05 Mike Gabriel

        * README.md: Add note that the license is LGPL-2.1+ (not just
          LGPL-2.1). (fe3ea2b)
        * Release 0.1.2 (18b3a20) (tag: 0.1.2)
        * README.md: Rephrase slightly to let it sound less clonky. (77a9f6f)
        * README.md: Don't contain RT's email address. (e8a8f03)
        * README.md, COPYING: Provide proper license and copyright
          attributions. (e82297e)
        * README.md: Drop all todos, they are done by done. (1dc5bc5)

2022-12-30 Mike Gabriel

        * Merge branch 'main_-_pincodeManager' into 'main' (7a1fd2e)

2022-11-09 Lionel Duboeuf

        * add AccountsService "PinCodePromptManager" property (10e5ade)

2022-12-14 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/session-migration' into 'main'
          (f17fd78)

2022-12-14 Ratchanan Srirattanamet

        * debian/*: ship session-migration scripts (9690796)
          Fixes:
          https://gitlab.com/ubports/development/core/lomiri-schemas/-/issues/1

2022-12-04 Mike Gabriel

        * Merge branch 'pr/add-lockscreen-shortcut' into 'main' (1790ee3)

2022-03-04 Robert Tari

        * data/schemas/com.lomiri.Shell.gschema.xml.in: Add lockscreen
          shortcut key (0dcbb95)

2022-11-27 Marius Gripsgard

        * Merge branch 'personal/fredldotme/gsettingsblur' into 'main'
          (f295d74)

2022-11-27 Alfred Neumayer

        * schemas: Shell schema setting for blurred drawer & indicator panels
          (0e6d955)

2022-11-10 Ratchanan Srirattanamet

        * Merge branch 'app-url-fixes' into 'main' (8475d5b)

2022-11-09 Guido Berhoerster

        * Fix renamed app IDs of core apps (eee373d)

2022-02-18 Mike Gabriel

        * Merge branch 'pr/fix-symlink-path' into 'main' (60e0cdb)

2022-02-18 Robert Tari

        * data/CMakeLists.txt: Fix symlink path (6a6d079)

2022-02-16 Mike Gabriel

        * Release 0.1.1 (df3aa5f) (tag: 0.1.1)

2022-02-10 Ratchanan Srirattanamet

        * Merge branch 'pr/add-pkg-config-file' into 'main' (96bfaa1)

2022-01-27 Robert Tari

        * Add a pkg-config file (193ac6e)

2022-01-18 Dalton Durst

        * Merge branch 'personal/peat-psuwit/fix-as-inf-links' into 'main'
          (4a27c8d)

2022-01-17 Ratchanan Srirattanamet

        * data/CMakeLists: fix the creation of AS interface links (0a4a672)

2021-11-18 Dalton Durst

        * Merge branch 'personal/peat-psuwit/remove-com-lomiri-shell' into
          'main' (866474c) (tag: 0.1.0)

2021-11-19 Ratchanan Srirattanamet

        * accountsservice: remove com.lomiri.shell.AccountsService (0e59ae8)

2021-11-18 Ratchanan Srirattanamet

        * Merge branch 'mr/add-ubuntu-schemas' into 'main' (dc79f96)

2021-11-04 Robert Tari

        * Add schema files used by Lomiri + move schemas to separate folders
          (feed0e4)

2021-11-03 Mike Gabriel

        * debian/control: Add B-D: libglib2.0-dev. (c3aac5f)
        * initial commit for new src:pkg 'lomiri-schemas' (4c27df8)
